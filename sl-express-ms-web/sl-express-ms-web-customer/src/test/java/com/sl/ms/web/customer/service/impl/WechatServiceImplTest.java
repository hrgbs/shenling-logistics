package com.sl.ms.web.customer.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;

@SpringBootTest
class WechatServiceImplTest {

    @Resource
    WechatServiceImpl wechatService;

    @Test
    void getOpenid() {
        try {
            wechatService.getOpenid("0f1ynL0002F9DQ1Svp1005gpmo1ynL06");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //获取用户手机号
    @Test
    void getPhone() {
        try {
           String phone =  wechatService.getPhone("7e7676664911e909f1df7cb5e1c7a08584ca5de9bf18fae57bf10865f74fdce3");
            System.out.println(phone);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}