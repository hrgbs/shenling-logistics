package com.sl.ms.web.driver.service.impl;

import com.itheima.auth.sdk.AuthTemplate;
import com.itheima.auth.sdk.common.Result;
import com.itheima.auth.sdk.dto.LoginDTO;
import com.sl.ms.web.driver.service.LoginService;
import com.sl.ms.web.driver.vo.request.AccountLoginVO;
import com.sl.transport.common.exception.SLWebException;
import com.sl.transport.common.util.ObjectUtil;
import com.sl.transport.common.vo.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LoginServiceImpl implements LoginService {
    @Resource
    private AuthTemplate authTemplate;

    @Override
    public String accountLogin(AccountLoginVO accountLoginVO) {
        //调用权限管家接口，传递用户名和密码
        Result<LoginDTO> result = this.authTemplate.opsForLogin().token(accountLoginVO.getAccount(), accountLoginVO.getPassword());
        if (ObjectUtil.equal(result.getCode(),0)){
            //登录成功
            R.success(result.getData());
            return result.getData().toString();
        }
        //登录失败
        throw new SLWebException(result.getMsg());
    }
}
