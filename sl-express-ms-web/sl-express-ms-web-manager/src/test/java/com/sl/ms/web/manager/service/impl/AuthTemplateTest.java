package com.sl.ms.web.manager.service.impl;

import cn.hutool.json.JSONUtil;
import com.itheima.auth.boot.autoconfigure.AuthorityProperties;
import com.itheima.auth.sdk.AuthTemplate;
import com.itheima.auth.sdk.common.Result;

import com.itheima.auth.sdk.dto.AuthUserInfoDTO;
import com.itheima.auth.sdk.dto.LoginDTO;
import com.itheima.auth.sdk.service.TokenCheckService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class AuthTemplateTest {

    @Resource
    private AuthTemplate authTemplate;
    @Resource
    private TokenCheckService tokenCheckService;
    @Autowired
    private AuthorityProperties authorityProperties;

    @Test
    public void testLogin() {
        Result<LoginDTO> result = this.authTemplate.opsForLogin().token("shenlingadmin", "123456");
        System.out.println(result.getData().getUser());
        System.out.println(result.getData().getToken().getToken());
    }

    @Test
    public void checkToken() {
        //解析token
        //admin
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxMDI0NzA1NzA5MjU1NzczMzQ1IiwiYWNjb3VudCI6InNoZW5saW5nYWRtaW4iLCJuYW1lIjoi56We6aKG566h55CG5ZGYIiwib3JnaWQiOjEwMjQ3MDQ4NDQ0ODY3NTY2NDEsInN0YXRpb25pZCI6MTAyNDcwNTQ4OTQzNjQ5NDcyMSwiYWRtaW5pc3RyYXRvciI6ZmFsc2UsImV4cCI6MTY5Mzg0MjY4OH0.XAZkfYb8Lot-72eHPkl-ozxq3oZ6WG2igEII1Y2jaOE1M3K5qVYtElIE2S-6n_1gl49Oe9qf9EwEwCPV2twJdA";
        // String token = "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI5ODQxMjUyNTk4MzE1ODU4ODkiLCJhY2NvdW50IjoiY291cmllciIsIm5hbWUiOiLlv6vpgJLlkZgiLCJvcmdpZCI6OTc3Mjc0OTMzMDIzMzQ0MDMzLCJzdGF0aW9uaWQiOjk4MTIyMzcwMzMzNTQxMDYyNSwiYWRtaW5pc3RyYXRvciI6ZmFsc2UsImV4cCI6MTY1NTE1NjY1NX0.jNLni8UYqQ0ZxHLNFVz4sTh_emrCSMKTQyltc77B5z7sHEEAsC0qUgdWUARmnQyp0NskeUKQtWpSgWZ_T1ULKQ";
        //eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxMDI0NzA1NzA5MjU1NzczMzQ1IiwiYWNjb3VudCI6InNoZW5saW5nYWRtaW4iLCJuYW1lIjoi56We6aKG566h55CG5ZGYIiwib3JnaWQiOjEwMjQ3MDQ4NDQ0ODY3NTY2NDEsInN0YXRpb25pZCI6MTAyNDcwNTQ4OTQzNjQ5NDcyMSwiYWRtaW5pc3RyYXRvciI6ZmFsc2UsImV4cCI6MTY5Mzg0MTczMn0.AFYDn37lJcYKAQRtZlj7RxmNKkOyFbPcav4keLdS6F8HuD2qP1lE-eLwrOPNLhD0s0dd6DADFm5Va_-ZirfVjg
        AuthUserInfoDTO authUserInfo = this.tokenCheckService.parserToken(token);
        System.out.println(authUserInfo);

        System.out.println(JSONUtil.toJsonStr(authUserInfo));

    }
}
