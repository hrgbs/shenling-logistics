package com.sl.ms.web.manager.service.impl;


import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.text.CharSequenceUtil;

import cn.hutool.core.util.StrUtil;
import com.itheima.auth.sdk.AuthTemplate;
import com.itheima.auth.sdk.common.Result;
import com.itheima.auth.sdk.common.Token;
import com.itheima.auth.sdk.dto.*;
import com.sl.ms.base.api.common.WorkSchedulingFeign;
import com.sl.ms.base.domain.base.WorkSchedulingDTO;
import com.sl.ms.base.domain.enums.StatusEnum;
import com.sl.ms.base.domain.enums.WorkUserTypeEnum;
import com.sl.ms.web.manager.service.AuthService;
import com.sl.ms.web.manager.vo.agency.AgencySimpleVO;
import com.sl.ms.web.manager.vo.auth.CourierVO;
import com.sl.ms.web.manager.vo.auth.SysUserVO;
import com.sl.transport.common.exception.SLWebException;
import com.sl.transport.common.util.AuthTemplateThreadLocal;
import com.sl.transport.common.util.ObjectUtil;
import com.sl.transport.common.util.PageResponse;
import com.sl.transport.common.vo.R;
import lombok.extern.slf4j.Slf4j;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;
/**
 * 鉴权服务
 * 登录 验证码 员工列表 快递员列表 角色
 */
@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    @Value("${role.courier}")
    private String roleId;
    @Resource
    private WorkSchedulingFeign workSchedulingFeign;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    private static final String CAPTCHA_REDIS_PREFIX = "CAPTCHA_";

    @Resource
    private AuthTemplate authTemplate;

    /**
     * 登录
     *
     * @param login 用户登录信息
     * @return 登录结果
     */
    @Override
    public R<LoginDTO> login(LoginParamDTO login) {
        //校验参数
         if (ObjectUtil.hasEmpty(login.getKey(),login.getCode())){
             throw new SLWebException("验证码错误");
         }
        if (ObjectUtil.hasEmpty(login.getAccount(),login.getPassword())){
            throw new SLWebException("用户名密码不能为空");
        }

        //校验验证码
        String redisKey = CAPTCHA_REDIS_PREFIX + login.getKey();
        String redisVlaus = stringRedisTemplate.opsForValue().get(redisKey);
        this.stringRedisTemplate.delete(redisKey);
        boolean verify = new MathGenerator().verify(redisVlaus, login.getCode());
        if (!verify){
           throw new SLWebException("验证码错误!");
        }

        //登录
        R<LoginDTO> result = this.login(login.getAccount(), login.getPassword());
        return result;
    }

    /**
     * 登录获取token
     * @param account  账号
     * @param password 密码
     * @return 登录信息
     */
    @Override
    public R<LoginDTO> login(String account, String password) {
     /*   //说明：由于后台系统的账号在后面会由【权限管家】系统中管理，由于【权限管家】目前还没学习，所以这里的登录先做【模拟实现】
        if (!(StrUtil.equals(account, "shenlingadmin") && StrUtil.equals(password, "123456"))) {
            return R.error("用户名或密码错误");
        }

        LoginDTO loginDTO = new LoginDTO();

        //设置token
        Token token = new Token();
        token.setToken("eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxMDI0NzA1NzA5MjU1NzczMzQ1IiwiYWNjb3VudCI6InNoZW5saW5nYWRtaW4iLCJuYW1lIjoi56We6aKG566h55CG5ZGYIiwib3JnaWQiOjEwMjQ3MDQ4NDQ0ODY3NTY2NDEsInN0YXRpb25pZCI6MTAyNDcwNTQ4OTQzNjQ5NDcyMSwiYWRtaW5pc3RyYXRvciI6ZmFsc2UsImV4cCI6MTY4MDc5NjE5OX0.W4RrB4p5YmjgEcdyGbbL4UrdWFirFbUu_e8Pgwxgr6vBVnj5z40JcFG4X3nIbrIXcSXUldi6oEuNfqAtZ9dUUw");
        token.setExpire(9999);
        loginDTO.setToken(token);

        //设置用户信息
        UserDTO userDTO = new UserDTO();
        userDTO.setAccount(account);
        userDTO.setName("神领管理员");
        //其它属性暂时不设置
        loginDTO.setUser(userDTO);

        return R.success(loginDTO);*/
        Result<LoginDTO> result = this.authTemplate.opsForLogin().token(account, password);
        if (ObjectUtil.equal(result.getCode(),0)){
            //登录成功
            return R.success(result.getData());
        }
        throw new SLWebException(result.getMsg());
    }



    //测试推送
    @Override
    public void createCaptcha(String key, HttpServletResponse response) throws IOException {
        //生成验证码
       ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(115, 42, 4, 4);
        //自定义验证码方式为四则运算方式
        captcha.setGenerator(new MathGenerator(1));
        //重新生成code
        String code = captcha.getCode();
        //保存验证码的到Redis中
        String redisKey = CAPTCHA_REDIS_PREFIX + key ;
        stringRedisTemplate.opsForValue().set(redisKey,code);
        //设置返回
        response.setHeader(HttpHeaders.PRAGMA, "No-cache");
        response.setHeader(HttpHeaders.CACHE_CONTROL, "No-cache");
        response.setDateHeader(HttpHeaders.EXPIRES, 0L);
        captcha.write(response.getOutputStream());
    }

    @Override
    public boolean check(String key, String value) {
        //TODO 待实现
        return false;
    }

    /**
     * 转换用户
     *
     * @param userDTO 用户DTO
     * @return 用户VO
     */
    @Override
    public SysUserVO parseUser2Vo(UserDTO userDTO) {
        SysUserVO vo = new SysUserVO();
        //填充基本信息
        vo.setUserId(userDTO.getId());
        vo.setAvatar(userDTO.getAvatar());
        vo.setEmail(userDTO.getEmail());
        vo.setMobile(userDTO.getMobile());
        vo.setAccount(userDTO.getAccount());
        vo.setName(userDTO.getName());
        vo.setStatus(userDTO.isStatus() ? StatusEnum.NORMAL.getCode() : StatusEnum.DISABLED.getCode());

        //处理所属机构信息
        AgencySimpleVO agency = new AgencySimpleVO();
        agency.setName(userDTO.getOrgName());
        vo.setAgency(agency);

        //处理岗位信息
        vo.setStationName(userDTO.getStationName());
        // 角色
        vo.setRoleNames(userDTO.getRoleNames());
        return vo;
    }

    /**
     * 获取用户信息
     *
     * @param id 用户id
     * @return 执行结果
     */
    @Override
    public SysUserVO user(Long id) {
        Result<UserDTO> result = AuthTemplateThreadLocal.get().opsForUser().getUserById(id);
        if (result.getCode() != 0) {
            return new SysUserVO();
        }
        return parseUser2Vo(result.getData());
    }

    /**
     * 批量获取用户信息
     *
     * @param ids 用户id
     * @return 执行结果
     */
    @Override
    public List<SysUserVO> users(List<Long> ids) {
        List<Long> longList = ids.stream().filter(Objects::nonNull).collect(Collectors.toList());
        Result<List<UserDTO>> result = AuthTemplateThreadLocal.get().opsForUser().list(longList);
        if (result.getCode() != 0) {
            return new ArrayList<>();
        }
        return result.getData().parallelStream().map(this::parseUser2Vo).collect(Collectors.toList());
    }

    /**
     * 员工分页
     *
     * @param page     页数
     * @param pageSize 页大小
     * @param agencyId 机构ID
     * @return 员工列表
     */
    @Override
    public PageResponse<SysUserVO> findUserByPage(Integer page, Integer pageSize, Long agencyId, String account, String name, String mobile) {
        Result<PageDTO<UserDTO>> result = AuthTemplateThreadLocal.get().opsForUser().getUserByPage(new UserPageDTO(page, pageSize, account, name, ObjectUtil.isNotEmpty(agencyId) ? agencyId : null, mobile));
        return getPageResponseR(page, pageSize, result);
    }

    /**
     * 快递员分页
     *
     * @param page     页数
     * @param pageSize 页大小
     * @param name     名称
     * @param mobile   手机号
     * @return 快递员列表
     */
    @Override
    public PageResponse<SysUserVO> findCourierByPage(Integer page, Integer pageSize, String name, String mobile, String account, Long orgId) {
        UserPageDTO userPageDTO = new UserPageDTO(page, pageSize, account, name, orgId, mobile);
        userPageDTO.setRoleId(roleId);
        Result<PageDTO<UserDTO>> result = AuthTemplateThreadLocal.get().opsForUser().getUserByPage(userPageDTO);

        // 转换vo
        PageResponse<SysUserVO> pageResponseR = getPageResponseR(page, pageSize, result);
        if (CollUtil.isEmpty(pageResponseR.getItems())) {
            return pageResponseR;
        }

        List<Long> userIds = pageResponseR.getItems().parallelStream().map(SysUserVO::getUserId).collect(Collectors.toList());
        if (CollUtil.isEmpty(userIds)) {
            return pageResponseR;
        }

        // 补充数据
        String bidStr = CollUtil.isEmpty(userIds) ? "" : CharSequenceUtil.join(",", userIds);
        List<WorkSchedulingDTO> workSchedulingDTOS = workSchedulingFeign.monthSchedule(bidStr, null, WorkUserTypeEnum.COURIER.getCode(), LocalDateTimeUtil.toEpochMilli(LocalDateTimeUtil.now()));
        if (CollUtil.isEmpty(workSchedulingDTOS)) {
            return pageResponseR;
        }
        Map<Long, Boolean> workMap = workSchedulingDTOS.parallelStream().filter(workSchedulingDTO -> ObjectUtil.isNotEmpty(workSchedulingDTO.getWorkSchedules())).collect(Collectors.toMap(WorkSchedulingDTO::getUserId, workSchedulingDTO -> workSchedulingDTO.getWorkSchedules().get(0)));

        pageResponseR.getItems().parallelStream().forEach(userDTO -> {
            // 上班状态
            try {
                Boolean aBoolean = workMap.get(userDTO.getUserId());
                if (ObjectUtil.isNotEmpty(aBoolean)) {
                    userDTO.setWorkStatus(aBoolean ? 1 : 0);
                }
            } catch (Exception ignored) {
                log.info("Exception:{}", ignored.getMessage());
            }
        });
        return pageResponseR;
    }

    /**
     * 转换用户返回结果
     *
     * @param page     页数
     * @param pageSize 页大小
     * @param result   用户信息
     * @return 用户信息
     */
    private PageResponse<SysUserVO> getPageResponseR(@RequestParam(name = "page") Integer page, @RequestParam(name = "pageSize") Integer pageSize, Result<PageDTO<UserDTO>> result) {
        if (result.getCode() == 0 && ObjectUtil.isNotEmpty(result.getData())) {
            PageDTO<UserDTO> userPage = result.getData();
            //处理对象转换
            List<SysUserVO> voList = userPage.getRecords().parallelStream().map(this::parseUser2Vo).collect(Collectors.toList());
            return PageResponse.of(voList, page, pageSize, userPage.getTotal() % userPage.getSize(), userPage.getTotal());
        }
        return PageResponse.getInstance();
    }

    /**
     * 根据机构查询快递员
     *
     * @param agencyId 机构id
     * @return 快递员列表
     */
    @Override
    public List<CourierVO> findByAgencyId(Long agencyId) {
        //构件查询条件
        UserPageDTO userPageDTO = new UserPageDTO(1, 999, null, null, agencyId, null);
        userPageDTO.setRoleId(roleId);

        //分页查询
        Result<PageDTO<UserDTO>> result = AuthTemplateThreadLocal.get().opsForUser().getUserByPage(userPageDTO);
        if (ObjectUtil.isEmpty(result.getData().getRecords())) {
            return Collections.emptyList();
        }

        //组装响应结果
        return result.getData().getRecords().stream().map(userDTO -> BeanUtil.toBean(userDTO, CourierVO.class)).collect(Collectors.toList());
    }


}
