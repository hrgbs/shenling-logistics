package com.sl.ms.web.courier.service.impl;

import com.itheima.auth.sdk.AuthTemplate;
import com.itheima.auth.sdk.common.Result;
import com.itheima.auth.sdk.common.Token;
import com.itheima.auth.sdk.dto.LoginDTO;
import com.sl.ms.web.courier.service.LoginService;
import com.sl.ms.web.courier.vo.login.AccountLoginVO;
import com.sl.ms.web.courier.vo.login.LoginVO;
import com.sl.transport.common.exception.SLWebException;
import com.sl.transport.common.util.ObjectUtil;
import com.sl.transport.common.vo.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    @Resource
    private AuthTemplate authTemplate;

    /**
     * 根据用户名和密码进行登录
     *
     * @param accountLoginVO 登录信息
     * @return token
     */
    @Override
    public R<LoginVO> accountLogin(AccountLoginVO accountLoginVO) {
        //调用权限管家接口，传递用户名和密码
        Result<LoginDTO> result = this.authTemplate.opsForLogin().token(accountLoginVO.getAccount(), accountLoginVO.getPassword());
        if (ObjectUtil.equal(result.getCode(),0)){
            //登录成功
            LoginVO loginResult = new LoginVO();
            loginResult.setToken(result.getData().getToken().getToken());
            return R.success(loginResult);
        }
        throw  new SLWebException(result.getMsg());
    }
}
