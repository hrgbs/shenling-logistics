package com.sl.gitflow.controller;

import com.sl.transport.common.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "GitFlow示例")
@RestController
@RequestMapping("/gitflow")
public class GitFlowController {


    //加法计算
    @ApiOperation("两个数相加")
    @GetMapping("/add")
    @ApiImplicitParams({@ApiImplicitParam(name = "value1", value = "第一个数", example = "1"), @ApiImplicitParam(name = "value2", value = "第两个数", example = "2")})
    public R<Object> add(@RequestParam(value = "value1") Long value1, @RequestParam(value = "value2") Long value2) {
        return R.success(value1 + value2);
    }


    //乘法计算
    @ApiOperation("两个数相乘")
    @GetMapping("/mul")
    @ApiImplicitParams({@ApiImplicitParam(name = "value1", value = "第一个数", example = "1"), @ApiImplicitParam(name = "value2", value = "第两个数", example = "2")})
    public R<Object> mul(@RequestParam(value = "value1") Long value1, @RequestParam(value = "value2") Long value2) {
        return R.success(value1 * value2);
    }

    //乘法计算
    @ApiOperation("两个数相减")
    @GetMapping("/jian")
    @ApiImplicitParams({@ApiImplicitParam(name = "value1", value = "第一个数", example = "1"), @ApiImplicitParam(name = "value2", value = "第两个数", example = "2")})
    public R<Object> jian(@RequestParam(value = "value1") Long value1, @RequestParam(value = "value2") Long value2) {
        return R.success(value1 - value2);
    }


}
